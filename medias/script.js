

$(function(){
   
    var typed =new Typed('#dialogue', {
            strings: ['<h3>  Bienvenue sur Number-Mind, un jeu dans lequel je vais devoir deviner à quel nombre tu penses ! </h3>  <h4 id="regles">  Les règles sont très simples : </h4>'+
            '<ul>'+
              "<li>Tout d'abord tu vas devoir penser à un nombre compris  <strong> entre 1 et 100. </strong></li> <br />"+
              '<li>Ensuite je vais te donner une série de cartes avec plusieurs nombres inscrits. Tu devras dire avec le bouton "oui" si le nombre est présent, sinon avec le bouton "non"'+" tu m'indiqueras que ton nombre n'est pas présent sur la carte.</li> <br />"+
              '<li>Et... à la fin je te promets de trouver ton nombre à coup sûr ;)   </li> <br />'+
           '</ul>'+
           '<img id="fin"class="quote" src="./medias/img/quoteFin.png" alt="quoteFin">'+
        ' <button id="boutonAccueil" onclick="afficherCarte1Oui()" >Commencer !</button>' ],
           typeSpeed: 0,
           showCursor: false,


        });
  cartes();
  cacher();



});


function cartes()
{
    var carte1 = new Array();
    var j = 0;
    for(h=1;h<=99;h=h+2){
        carte1[j]=h;
        j++;
    }
            var l=0;
            var id=0;
            $("#carte1 table").append('<tr id="0">');
            for(i=0;i<carte1.length;i++)
            {
                n = carte1[i];
                $("#carte1 #"+id).append("<td>"+n+"</td>");
                    l++;
                if(l%9==0){
                    id++;
                    $("#carte1 table").append("</tr><tr id='"+id+"'>");
                }
              }

    var carte2 = new Array();

    res=2;
    carte2[0]=res;
    for(i=1;i<50;i++){
        if(i%2!=0){
            carte2[i]=res+1;
            res=carte2[i];
        }
        else
        {
            carte2[i]=res+3;
            res=carte2[i];
        }

    }

            var l=0;
            var id=0;
            $("#carte2 table").append('<tr id="0">');
            for(i=0;i<carte2.length;i++)
            {
                n = carte2[i];
                $("#carte2 #"+id).append("<td>"+n+"</td>");
                    l++;
                if(l%9==0){
                    id++;
                    $("#carte2 table").append("</tr><tr id='"+id+"'>");
                }

            }


    var carte3 = new Array();

    var count = 1;
    res=4;
    carte3[0]=res;

    for(i=1;i<49;i++){
        if(count%4!=0)
        {
            carte3[i]=res+1;
            res=carte3[i];
        }
        else{
            carte3[i]=res+5;
            res=carte3[i];
        }
        count+=1;
    }

            var l=0;
            var id=0;
            $("#carte3 table").append('<tr id="0">');
            for(i=0;i<carte3.length;i++)
            {
                n = carte3[i];
                $("#carte3 #"+id).append("<td>"+n+"</td>");
                    l++;
                if(l%9==0){
                    id++;
                    $("#carte3 table").append("</tr><tr id='"+id+"'>");
                }

            }
            var carte4 = new Array();

            var count = 1;
            res=8;
            carte4[0]=res;

            for(i=1;i<48;i++){
                if(count%8!=0)
                {
                    carte4[i]=res+1;
                    res=carte4[i];
                }
                else{
                    carte4[i]=res+9;
                    res=carte4[i];
                }
                count+=1;
            }

        var l=0;
        var id=0;
        $("#carte4 table").append('<tr id="0">');
        for(i=0;i<carte4.length;i++)
        {
            n = carte4[i];
            $("#carte4 #"+id).append("<td>"+n+"</td>");
                l++;
            if(l%9==0){
                id++;
                $("#carte4 table").append("</tr><tr id='"+id+"'>");
            }

        }

            var carte5 = new Array();

            var count = 1;
            res=16;
            carte5[0]=res;

            for(i=1;i<48;i++){
                if(count%16!=0)
                {
                    carte5[i]=res+1;
                    res=carte5[i];
                }
                else{
                    carte5[i]=res+17;
                    res=carte5[i];
                }
                count+=1;
            }

                    var l=0;
                    var id=0;
                    $("#carte5 table").append('<tr id="0">');
                    for(i=0;i<carte5.length;i++)
                    {
                        n = carte5[i];
                        $("#carte5 #"+id).append("<td>"+n+"</td>");
                            l++;
                        if(l%9==0){
                            id++;
                            $("#carte5 table").append("</tr><tr id='"+id+"'>");
                        }

                    }

        var carte6 = new Array();

        var count = 1;
        res=32;
        carte6[0]=res;

        for(i=1;i<37;i++){
            if(count%32!=0)
            {
                carte6[i]=res+1;
                res=carte6[i];
            }
            else{
                carte6[i]=res+33;
                res=carte6[i];
            }
            count+=1;
        }

            var l=0;
            var id=0;
            $("#carte6 table").append('<tr id="0">');
            for(i=0;i<carte6.length;i++)
            {
                n = carte6[i];
                $("#carte6 #"+id).append("<td>"+n+"</td>");
                    l++;
                if(l%9==0){
                    id++;
                    $("#carte6 table").append("</tr><tr id='"+id+"'>");
                }

            }


    var carte7 = new Array();

    var j = 0;
    for(i=64;i<=100;i++){
        carte7[j]=i;
        j++;
    }

        var l=0;
        var id=0;
        $("#carte7 table").append('<tr id="0">');
        for(i=0;i<carte7.length;i++)
        {
            n = carte7[i];
            $("#carte7 #"+id).append("<td>"+n+"</td>");
                l++;
            if(l%9==0){
                id++;
                $("#carte7 table").append("</tr><tr id='"+id+"'>");
            }

        }





}
    var resultat = 0;



  function  cacher()
    {
        $('#regle button').hide();
        $('#carte1').hide();
        $('#carte2').hide();
        $('#carte3').hide();
        $('#carte4').hide();
        $('#carte5').hide();
        $('#carte6').hide();
        $('#carte7').hide();
        $('#resultat').hide();
    }

    function afficherCarte1Oui()
    {
        $('#regle button').show();
        $('#dialogueAccueil').hide();
        $('#carte1').show();
    }

    function afficherCarte2Oui()
    {
        resultat+=1;
        $('#carte1').hide();
        $('#carte2').show();
    }

    function afficherCarte2Non()
    {
        $('#carte1').hide();
        $('#carte2').show();
    }

    function afficherCarte3Oui()
    {
        resultat+=2;
        $('#carte2').hide();
        $('#carte3').show();
    }

    function afficherCarte3Non()
    {
        $('#carte2').hide();
        $('#carte3').show();
    }

    function afficherCarte4Oui()
    {
        resultat+=4;
        $('#carte3').hide();
        $('#carte4').show();
    }

    function afficherCarte4Non()
    {
        $('#carte3').hide();
        $('#carte4').show();
    }

    function afficherCarte5Oui()
    {
        resultat+=8;
        $('#carte4').hide();
        $('#carte5').show();
    }

    function afficherCarte5Non()
    {
        $('#carte4').hide();
        $('#carte5').show();
    }

    function afficherCarte6Oui()
    {
        resultat+=16;
        $('#carte5').hide();
        $('#carte6').show();
    }

    function afficherCarte6Non()
    {
        $('#carte5').hide();
        $('#carte6').show();
    }

    function afficherCarte7Oui()
    {
        resultat+=32;
        $('#carte6').hide();
        $('#carte7').show();
    }

    function afficherCarte7Non()
    {
        $('#carte6').hide();
        $('#carte7').show();
    }

    function afficherResultatOui()
    {
        resultat+=64;

      if (resultat>100)
      {
        $('#carte7').hide();
        $('#resultat').show();
        $("#resultat").html("<p> <img class='quote' src='./medias/img/quoteDebut.png' alt='quoteDebut'> Tu pensais m'avoir eu mais je sais que tu n'as pas respecté les règles" +" <img class='quote' src='./medias/img/quoteFin.png' alt='quoteFin'> </p>  <br /> <br />   <button onclick='recommencer()'>Recommencer</button>");
      }
      else
        {
          $('#carte7').hide();
          $('#resultat').show();
          $("#resultat").html("<p> <img class='quote' src='./medias/img/quoteDebut.png' alt='quoteDebut'> Vous pensez au nombre "+resultat+" <img class='quote' src='./medias/img/quoteFin.png' alt='quoteFin'> </p>  <br /> <br />   <button onclick='recommencer()'>Recommencer</button> ");
        }

      }

    function afficherResultatNon()
    {

      if (resultat==0)
      {
        $('#carte7').hide();
        $('#resultat').show();
        $("#resultat").html("<p> <img class='quote' src='./medias/img/quoteDebut.png' alt='quoteDebut'> Tu pensais m'avoir eu mais je sais que tu n'as pas respecté les règles " +" <img class='quote' src='./medias/img/quoteFin.png' alt='quoteFin'> </p>  <br /> <br />   <button onclick='recommencer()'>Recommencer</button> ");
      }
      else
        {
          $('#carte7').hide();
          $('#resultat').show();
          $("#resultat").html("<p> <img class='quote' src='./medias/img/quoteDebut.png' alt='quoteDebut'> Vous pensez au nombre "+resultat+" <img class='quote' src='./medias/img/quoteFin.png' alt='quoteFin'> </p>  <br /> <br />   <button onclick='recommencer()'>Recommencer</button> ");
        }


    }

    function recommencer()
    {
      $('#resultat').hide();
      resultat=0;
      $('#carte1').show();
    }

    function recommencerRegles()
    {
        cacher();
      $('#resultat').hide();
      resultat=0;
      $('#dialogueAccueil').show();
    }
