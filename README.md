# Number Mind

Number Mind est un jeu de carte dans lequel un renard mentaliste devra deviner le nombre auquel tu penses. Ce nombre doit être compris entre 1 et 100, pour cela le renard te proposera différentes grilles remplis de nombre et tu devras lui indiquer si ton nombre se trouve sur cette grille ou non.


![Image Renard](https://gitlab.com/aslaville/number-mind/uploads/5027aa149af9ae3492d20adb1d45755e/RenardMind.bmp)

## Pour commencer

Ce n'est pas très compliqué, pour commencer il vous faudra simplement un navigateur web et vous rendre sur https://number-mind.now.sh/



## Auteurs

* **Anne-Sophie Lavile** - *Développeuse* - [aslaville](https://gitlab.com/aslaville)
* **Nicolas Pattyn** - *Développeur* - [nikhn](https://gitlab.com/nikhn)



## Outil de développement

* [Javascript]
* [HTML]
* [CSS]
